import { useContext, useEffect } from "react"
import authContext from "../../context/auth/authContext"

export default function Barra() {
    
    //Extraer la información de autenticación
    const authsContext = useContext(authContext)
    const { usuario, usuarioAutenticado, cerrarSesion } = authsContext
    
    useEffect(() => {
        usuarioAutenticado()
        //eslint-disable-next-line
    }, [])

    const handleLogout = () => {
        if(window.confirm("¿Cerrar Sesión?")){
            cerrarSesion()
        }
    }

    return (
        <header className="app-header">
            {usuario && (
                <p className="nombre-usuario">Hola <span>{usuario.nombre}</span></p>
            )}
            <nav className="nav-principal">
                <button className="btn btn-blank cerrar-sesion" onClick={() => handleLogout()}>Cerrar Sesión</button>
            </nav>
        </header>
    )
}
