import { useState, useContext, useEffect } from "react"
import { Link, useNavigate } from "react-router-dom"
import alertaContext from "../../context/alertas/alertaContext"
import authContext from "../../context/auth/authContext"

export default function Login() {
    let history = useNavigate()

    //extraer los valores del context
    const alertasContext = useContext(alertaContext)
    const { alerta, mostrarAlerta } = alertasContext

    const authsContext = useContext(authContext)
    const { mensaje, autenticado, iniciarSesion } = authsContext

    //En caso de que el usuario se haya autenticado / registro duplicado
    useEffect(() => {
        if(autenticado){
            history('/proyectos')
        }

        if(mensaje){
            mostrarAlerta(mensaje.msg, mensaje.categoria)
        }
        
        //eslint-disable-next-line
    }, [mensaje, autenticado])

    const [usuario, setUsuario] = useState({
        email: "",
        password: ""
    })

    const { email, password } = usuario

    const handleChange = e => {
        setUsuario({
            ...usuario,
            [e.target.name]: e.target.value
        })
    }

    //Iniciar sesion
    const handleSubmit = e => {
        e.preventDefault()

        //Validar
        if ([email, password].includes("")) {
            mostrarAlerta("Todos los campos son obligatorios", "error")
            return
        }

        //Pasarlo al action
        iniciarSesion(usuario)
    }

    return (
        <div className="form-usuario">

            {alerta && (
                <div className={`alerta ${alerta.categoria}`}>{alerta.msg}</div>
            )}

            <div className="contenedor-form sombra-dark">
                <h1>Iniciar Sesión</h1>

                <form
                    onSubmit={handleSubmit}
                >
                    <div className="campo-form">
                        <label htmlFor="email">Email</label>
                        <input type="email" name="email" id="email" placeholder="Tu Email" value={email} onChange={handleChange} />
                    </div>

                    <div className="campo-form">
                        <label htmlFor="password">Password</label>
                        <input type="password" name="password" id="password" placeholder="Tu Password" value={password} onChange={handleChange} />
                    </div>

                    <div className="campo-form">
                        <input type="submit" className="btn btn-primario btn-block" value="Iniciar Sesión" />
                    </div>
                </form>

                <Link to={"/nueva-cuenta"} className="enlace-cuenta">Obtener una cuenta</Link>
            </div>
        </div>

    )
}
