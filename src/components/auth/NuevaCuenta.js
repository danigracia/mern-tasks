import { useState, useContext, useEffect } from "react"
import { Link, useNavigate } from "react-router-dom"
import alertaContext from "../../context/alertas/alertaContext"
import authContext from "../../context/auth/authContext"

export default function NuevaCuenta () {
    let history = useNavigate()

    //extraer los valores del context
    const alertasContext = useContext(alertaContext)
    const { alerta, mostrarAlerta } = alertasContext

    const authsContext = useContext(authContext)
    const { mensaje, autenticado, registrarUsuario } = authsContext

    //En caso de que el usuario se haya autenticado / registro duplicado
    useEffect(() => {
        if(autenticado){
            history('/proyectos')
        }

        if(mensaje){
            mostrarAlerta(mensaje.msg, mensaje.categoria)
        }
        
        //eslint-disable-next-line
    }, [mensaje, autenticado])

    const [usuario, setUsuario] = useState({
        nombre: "",
        email: "",
        password: "",
        confirmar: ""
    })

    const { nombre, email, password, confirmar } = usuario

    const handleChange = e => {
        setUsuario({
            ...usuario,
            [e.target.name]: e.target.value
        })
    }

    //Iniciar sesion
    const handleSubmit = e => {
        e.preventDefault()

        //Validar que no haya campos vacios
        if ([nombre, email, password, confirmar].includes("")) {
            mostrarAlerta("Todos los campos son obligatorios", "error")
            return
        }

        //Validar password, minimo 6 caracteres
        if (password.length < 6) {
            mostrarAlerta("El password debe ser minimo de 6 caracteres", "error")
            return
        }

        //Validar passwords iguales
        if (password !== confirmar) {
            mostrarAlerta("Los passwords no son iguales", "error")
            return
        }

        //Pasarlo al action
        registrarUsuario({
            nombre,
            email,
            password
        })
    }

    return (
        <div className="form-usuario">

            {alerta && (
                <div className={`alerta ${alerta.categoria}`}>{alerta.msg}</div>
            )}

            <div className="contenedor-form sombra-dark">
                <h1>Obtener una cuenta</h1>

                <form
                    onSubmit={handleSubmit}
                >
                    <div className="campo-form">
                        <label htmlFor="nombre">Nombre</label>
                        <input type="text" name="nombre" id="nombre" placeholder="Tu Nombre" value={nombre} onChange={handleChange} />
                    </div>

                    <div className="campo-form">
                        <label htmlFor="email">Email</label>
                        <input type="email" name="email" id="email" placeholder="Tu Email" value={email} onChange={handleChange} />
                    </div>

                    <div className="campo-form">
                        <label htmlFor="password">Password</label>
                        <input type="password" name="password" id="password" placeholder="Tu Password" value={password} onChange={handleChange} />
                    </div>

                    <div className="campo-form">
                        <label htmlFor="confirmar">Confirmar Password</label>
                        <input type="password" name="confirmar" id="confirmar" placeholder="Repite tu password" value={confirmar} onChange={handleChange} />
                    </div>

                    <div className="campo-form">
                        <input type="submit" className="btn btn-primario btn-block" value="Registrarme" />
                    </div>
                </form>

                <Link to={"/"} className="enlace-cuenta">Ya tengo una cuenta</Link>
            </div>
        </div>

    )
}
