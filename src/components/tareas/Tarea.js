import {useContext} from "react"
import tareaContext from "../../context/tareas/tareaContext"

export default function Tarea({ tarea }) {

    //Extraer funciones tarea
    const tareasContext = useContext(tareaContext)
    const { eliminarTarea, obtenerTareas, actualizarTarea, setTareaActual } = tareasContext

    const handleEliminar = () => {
        eliminarTarea(tarea._id, tarea.proyecto)
        obtenerTareas(tarea.proyecto)
    }

    //Funcion que modifica el estado de la tarea
    const handleEstado = async () => {
        tarea.estado = !tarea.estado
        actualizarTarea(tarea)
    }

    //Editar tarea
    const handleEditar = () => {
        setTareaActual(tarea)
    }

    return (
        <li className="tarea sombra">
            <p>{tarea.nombre}</p>
            <div className="estado">
                {tarea.estado
                    ?
                    (
                        <button type="button" className="completo" onClick={() => handleEstado()}>Completo</button>
                    )
                    :
                    (
                        <button type="button" className="incompleto" onClick={() => handleEstado()}>Incompleto</button>
                    )
                }
            </div>

            <div className="acciones">
                <button type="button" className="btn btn-primario" onClick={() => handleEditar()}>Editar</button>

                <button type="button" className="btn btn-secundario" onClick={handleEliminar}>Eliminar</button>
            </div>
        </li>
    )
}
