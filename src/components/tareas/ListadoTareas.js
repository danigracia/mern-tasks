import { useContext } from "react"
import Tarea from "./Tarea"
import proyectoContext from "../../context/proyectos/proyectoContext"
import tareaContext from "../../context/tareas/tareaContext"

export default function ListadoTareas() {

    //Extraer proyecto actual
    const proyectosContext = useContext(proyectoContext)
    const { proyecto, eliminarProyecto } = proyectosContext

    //Obtener las tareas del proyecto
    const tareasContext = useContext(tareaContext)
    const { tareasproyecto } = tareasContext

    //Sino hay proyecto selecionado
    if (!proyecto) return <h2>Selecciona un proyecto</h2>

    //Array destructuring para extraer proyecto actual
    const [proyectoActual] = proyecto

    //Eliminar proyecto
    const handleEliminar = () => {
        eliminarProyecto(proyectoActual._id)
    }

    return (
        <>
            <h2>Proyecto: {proyectoActual.nombre}</h2>

            <ul className='listado-tareas'>
                {tareasproyecto.length === 0
                    ? (<li className="tarea"><p>No hay tareas</p></li>)

                    :
                    (
                        tareasproyecto.map(tarea => (
                            <Tarea key={tarea._id} tarea={tarea} />
                        ))
                    )
                }
            </ul>
            <button type="button" className="btn btn-eliminar" onClick={handleEliminar}>Eliminar Proyecto &times;</button>
        </>
    )
}
