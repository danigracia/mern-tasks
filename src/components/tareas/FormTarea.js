import { useContext, useState, useEffect } from "react"
import proyectoContext from "../../context/proyectos/proyectoContext"
import tareaContext from "../../context/tareas/tareaContext"

export default function FormTarea() {

    //Extraer proyecto actual
    const proyectosContext = useContext(proyectoContext)
    const { proyecto } = proyectosContext

    //Extraer funcion agregar tarea 
    const tareasContext = useContext(tareaContext)
    const { errortarea, tareaseleccionada, obtenerTareas, agregarTarea, validarTarea, actualizarTarea } = tareasContext

    //Escuchar cambios en tarea seleccionada (editar)
    useEffect(() => {
        if(tareaseleccionada){
            setTarea(tareaseleccionada)
        }
    }, [tareaseleccionada])

    //State del formulario
    const [tarea, setTarea] = useState({
        nombre: ""
    })
    const { nombre } = tarea

    //Sino hay proyecto selecionado
    if (!proyecto) return null

    //Array destructuring para extraer proyecto actual
    const [proyectoActual] = proyecto

    //Leer valores del formulario
    const handleChange = e => {
        setTarea({
            ...tarea,
            [e.target.name]: e.target.value
        })
    }

    const handleSubmit = e => {
        e.preventDefault()

        //validar
        if(nombre.trim() === ""){
            validarTarea()
            return
        }

        //Edición o Nueva
        if(tareaseleccionada){
            //Editando
            actualizarTarea(tarea)
        } else {
            //agregar nueva tarea
            tarea.proyecto = proyectoActual._id
            tarea.estado = false
            agregarTarea(tarea)
        }



        //Obtener las tareas (incluyendo la nueva)
        obtenerTareas(proyectoActual._id)

        //reiniciar el form
        setTarea({
            nombre: ""
        })
    }

    return (
        <div className='formulario'>
            <form
                onSubmit={handleSubmit}
            >
                <div className="contenedor-input">
                    <input type="text" name="nombre" className="input-text" placeholder="Nombre tarea" value={nombre} onChange={handleChange} />
                </div>
                <div className="contenedor-input">
                    <input type="submit" className="btn btn-primario btn-submit btn-block" value={tareaseleccionada ? "Editar Tarea" : "Agregar Tarea"} />
                </div>
            </form>

            {errortarea && (
                <p className="mensaje error">El nombre es obligatorio</p>
            )}
        </div>
    )
}
