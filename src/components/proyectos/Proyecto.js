import { useContext } from "react"
import proyectoContext from "../../context/proyectos/proyectoContext"
import tareaContext from "../../context/tareas/tareaContext"

export default function Proyecto({ proyecto }) {
    //Obtener state de proyectos
    const proyectosContext = useContext(proyectoContext)
    const { proyectoActual } = proyectosContext

    //Obtener state de tareas
    const tareasContext = useContext(tareaContext)
    const { obtenerTareas } = tareasContext

    //Agregar el proyecto actual
    const seleccionarProyecto = id => {
        proyectoActual(id) //Fijar un proyecto actual
        obtenerTareas(id) //Filtrar las tareas del proyecto actual
    }

    return (
        <li>
            <button type="button" className="btn btn-blank" onClick={() => seleccionarProyecto(proyecto._id)}>{proyecto.nombre}</button>
        </li>
    )
}
