import { useState, useContext } from "react"
import proyectoContext from "../../context/proyectos/proyectoContext"

export default function NuevoProyecto() {

    //Obtener state del formulario
    const proyectosContext = useContext(proyectoContext)
    const { formulario, errorformulario, mostrarFormulario, agregarProyecto, validarProyecto } = proyectosContext

    //State para proyecto
    const [proyecto, setProyecto] = useState({
        nombre: ""
    })

    const { nombre } = proyecto

    //Actualizar state
    const handleChange = e => {
        setProyecto({
            ...proyecto,
            [e.target.name]: e.target.value
        })
    }

    //Cuando el usuario añade un proyecto
    const handleSubmit = e => {
        e.preventDefault()

        //Validar proyecto
        if (nombre === "") {
            validarProyecto()
            return
        }

        //Añadir al state
        agregarProyecto(proyecto)

        //Reiniciar el form
        setProyecto({
            nombre: ""
        })
    }

    return (
        <>
            <button type="button" className="btn btn-block btn-primario" onClick={() => mostrarFormulario()}>Nuevo Proyecto</button>

            {formulario && (
                <form className="formulario-nuevo-proyecto" onSubmit={handleSubmit}>
                    <input type="text" name="nombre" id="nombre" className="input-text" placeholder="Nombre Proyecto" value={nombre} onChange={handleChange} />
                    <input type="submit" className="btn btn-block btn-primario" value="Agregar Proyecto" />
                </form>
            )}

            {errorformulario && (
                <p className="mensaje error">El nombre es obligatorio</p>
            )}
        </>

    )
}
