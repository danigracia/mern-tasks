import { useContext, useEffect } from "react"
import Proyecto from "./Proyecto"
import proyectoContext from "../../context/proyectos/proyectoContext"
import alertaContext from "../../context/alertas/alertaContext"
import { TransitionGroup, CSSTransition } from "react-transition-group"

export default function ListadoProyectos() {

    //Extraer proyectos del state inicial
    const proyectosContext = useContext(proyectoContext)
    const { mensaje, proyectos, obtenerProyectos } = proyectosContext

    //Extraer alerta context
    const alertasContext = useContext(alertaContext)
    const { alerta, mostrarAlerta } = alertasContext

    //Obtener los proyectos cuando carga el componente
    useEffect(() => {
        if (mensaje) {
            mostrarAlerta(mensaje.msg, mensaje.categoria)
        }

        obtenerProyectos()

        //eslint-disable-next-line
    }, [mensaje])

    //Comprobar si hay proyectos
    if (proyectos.length === 0) return <p>No hay proyectos, crea uno</p>

    return (
        <ul className="listado-proyectos">

            {alerta && (
                <div className={`alerta ${alerta.categoria}`}>{alerta.msg}</div>
            )}

            <TransitionGroup>
                {proyectos.map(proyecto => (
                    <CSSTransition key={proyecto._id} timeout={200} classNames="proyecto">
                        <Proyecto proyecto={proyecto} />
                    </CSSTransition>
                ))}
            </TransitionGroup>
        </ul>
    )
}
