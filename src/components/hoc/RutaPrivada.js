import { useContext, useEffect } from "react";
import { Navigate, Outlet } from 'react-router-dom';
import authContext from "../../context/auth/authContext";
import Spinner from "../Spinner"

export default function RutaPrivada() {

    const authsContext = useContext(authContext)
    const {autenticado, cargando, usuarioAutenticado} = authsContext

    useEffect( () => {
        usuarioAutenticado()
        //eslint-disable-next-line
    }, [])
    
    if(cargando) return <Spinner />

    return !autenticado ? <Navigate to="/" />  :  <Outlet />;
}