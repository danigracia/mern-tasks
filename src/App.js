import { BrowserRouter as Router, Routes, Route } from "react-router-dom"

import Login from "./components/auth/Login"
import NuevaCuenta from "./components/auth/NuevaCuenta"
import Proyectos from "./components/proyectos/Proyectos"

import ProyectoState from "./context/proyectos/proyectoState"
import TareaState from "./context/tareas/tareaState"
import AlertaState from "./context/alertas/alertaState"
import AuthState from "./context/auth/authState"

import tokenAuth from "./config/token"

import RutaPrivada from "./components/hoc/RutaPrivada"

//Revisar si tenemos un token
const token = localStorage.getItem("token")
if (token) {
    tokenAuth(token)
}

function App() {
    return (
        <ProyectoState>
            <TareaState>
                <AlertaState>
                    <AuthState>

                        {/* Router */}
                        <Router>
                            <Routes>
                                <Route path="/" element={<Login />} />
                                <Route path="nueva-cuenta" element={<NuevaCuenta />} />

                                {/* Rutas privadas */}
                                <Route exact path='/' element={<RutaPrivada />}>
                                    <Route exact path='proyectos' element={<Proyectos />} />
                                </Route>
                            </Routes>
                        </Router>
                        {/* End Router */}

                    </AuthState>
                </AlertaState>
            </TareaState>
        </ProyectoState>
    );
}

export default App;
